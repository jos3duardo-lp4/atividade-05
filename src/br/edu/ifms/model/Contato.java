package br.edu.ifms.model;

public class Contato {
	
	String nome;
	String email;
	String texto;
	
	public String getNome() {
		return nome;
	}
	public String getEmail() {
		return email;
	}
	public String getTexto() {
		return texto;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	

}
