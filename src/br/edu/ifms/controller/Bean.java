package br.edu.ifms.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import br.edu.ifms.model.Contato;


@SessionScoped
@ViewScoped
@ManagedBean
public class Bean {
	
	Contato contato;
	List<Contato> contatos;
	public Bean() {
		this.contato = new Contato();
		this.contatos = new ArrayList<Contato>();
	}
		
	public void enviar() {
		
		System.out.println("nome: " + contato.getNome());
	    System.out.println("email: " + contato.getEmail());
	    System.out.println("texto: " + contato.getTexto());
	    
	    
		contatos.add(contato);
		contato = new Contato();
		
	}
	
	public Contato getContato() {
		return contato;
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}

	public String sobre() {
		return "sobre?faces-redirect=true";
	}
	public String contato() {
		return "contato?faces-redirect=true";
	}

}
